/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather.db;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;
import weather.weather.domain.Forecast;
import javax.inject.Named;


@Named
@Singleton
public class ForecastDB {

    List<Forecast> forecasts;

    public ForecastDB() {
        forecasts = new ArrayList<>();
    }

    public List<Forecast> getAll() {
        return forecasts;
    }

    public void updateDatabase(List<Forecast> forecasts) {
        if (!forecasts.isEmpty()) {
            this.forecasts.clear();
        }
        this.forecasts.addAll(forecasts);
    }

}
