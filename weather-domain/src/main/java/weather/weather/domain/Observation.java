package weather.weather.domain;

import java.time.LocalDateTime;

public class Observation {
	private LocalDateTime time;
	private String location;
	private double temp;
	

	public Observation(LocalDateTime time, String location, double temp) {
		this.time = time;
		this.location = location;
		this.temp = temp;
	}
	
	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}
}
