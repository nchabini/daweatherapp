package weather.weather.domain;

import java.time.LocalDateTime;


public class Forecast {
	private LocalDateTime forecastDate;
	private String location;
	private double maxTemp;
	
	public Forecast(LocalDateTime forecastDate, String location, double maxTemp) {
		this.forecastDate = forecastDate;
		this.location = location;
		this.maxTemp = maxTemp;
	}

	public LocalDateTime getForecastDate() {
		return forecastDate;
	}

	public void setForecastDate(LocalDateTime forecasetDate) {
		this.forecastDate = forecasetDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(double maxTemp) {
		this.maxTemp = maxTemp;
	}
	
	
	

}
