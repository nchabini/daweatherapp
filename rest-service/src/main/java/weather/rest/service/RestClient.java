/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather.rest.service;

import com.google.gson.Gson;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import weather.weather.domain.Forecast;
import weather.weather.domain.Observation;


@RequestScoped
public class RestClient {

    private final String KEY = "&appid=01065c26fb5eeeab0e1a1d0f8f944e65";
    private final String OBSERVATION_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    private final String FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast?q=";

    public Observation getObservation(String location) {
        String jsonString = getResponse("observation", location).readEntity(String.class);
        Gson gson = new Gson();
        return parseObservation(location, gson.fromJson(jsonString, ObservationResponse.class));
    }

    public List<Forecast> loadWeatherForecasts(String location) {
        String jsonString = getResponse("forecast", location).readEntity(String.class);
        Gson gson = new Gson();
        return parseForecasts(location, gson.fromJson(jsonString, ForecastResponse.class));
    }

    private Response getResponse(String type, String location) {
        Client client = ClientBuilder.newClient();
        WebTarget target=null;
        if (type.equals("observation")) {
           target = client.target(OBSERVATION_URL + location + KEY);
        }
        else{
             target = client.target(FORECAST_URL + location + KEY+"&cnt=3");
        }
        Builder builder = target.request();
        return  builder.get();
    }

    private Observation parseObservation(String location, ObservationResponse response) {
        return new Observation(LocalDateTime.now(), location, kelToCel(response.getMain().getTemp()));
    }

    private List<Forecast> parseForecasts(String location, ForecastResponse response) {
        List<Forecast> forecasts = new ArrayList<>();
        List<ForecastDetails> details = response.getList();
        for (ForecastDetails detail : details) {
            LocalDateTime date = unixTimestampConverter(detail.getDt());
            double cel = kelToCel(detail.getMain().getTemp());
            forecasts.add(new Forecast(date, location, cel));
        }
        return forecasts;
    }

    private double kelToCel(double kel) {
        return Math.round((kel - 273.15) * 100.0) / 100.0;
    }

    private LocalDateTime unixTimestampConverter(long unix) {
        Timestamp stamp = new Timestamp(unix*1000L);
        return stamp.toLocalDateTime();
    }

    private class ObservationResponse {

        private OpenweathermapMain main;

        public OpenweathermapMain getMain() {
            return main;
        }

        public void setMain(OpenweathermapMain main) {
            this.main = main;
        }

    }

    private class ForecastResponse {

        private List<ForecastDetails> list;

        public List<ForecastDetails> getList() {
            return list;
        }

        public void setList(List<ForecastDetails> list) {
            this.list = list;
        }
    }

    private class ForecastDetails {

        private long dt;
        private OpenweathermapMain main;

        public void setDt(long dt) {
            this.dt = dt;
        }

        public long getDt() {
            return dt;
        }

        public void setMain(OpenweathermapMain main) {
            this.main = main;
        }

        public OpenweathermapMain getMain() {
            return main;
        }
    }

    private class OpenweathermapMain {

        private double temp;

        public double getTemp() {
            return temp;
        }

        public void setTemp(double temp) {
            this.temp = temp;
        }
    }
}
