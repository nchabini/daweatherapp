/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather.rest.service;

import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import weather.db.ForecastDB;
import weather.weather.domain.Forecast;
import weather.weather.domain.Observation;

@Stateless
public class ForecastService {

    @Inject
    private RestClient restClient;

    @Inject
    private ForecastDB database;

    public Observation getObservation(String location) {
        return restClient.getObservation(location);
    }

    public List<Forecast> getForecasts(String location) {
        return database.getAll();
    }

    @Schedule(minute = "*/1", hour = "*", persistent = false)
    public void loadWeather() {

        List<Forecast> forecasts = restClient.loadWeatherForecasts("Leuven");
        System.out.println("Getting it. Size: " + forecasts.size());
        database.updateDatabase(forecasts);

    }
}
