/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather.rest.service;

import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import weather.weather.domain.Forecast;

@Path("/weather")
public class RestProvider {

    @EJB
    private ForecastService forecastService;

    @GET
    @Produces("text/html")
    @Path("/observation/{location}")
    public String getObservation(@PathParam("location") String location) {
        return "It is " + forecastService.getObservation(location).getTemp() + " degrees now in " + location;
    }

    @GET
    @Produces("text/html")
    @Path("forecast/{location}")
    public String getForecast(@PathParam("location") String location) {
        String result = "We expect:\n";
        List<Forecast> forecasts = forecastService.getForecasts(location);
        for (Forecast forecast : forecasts) {
            result += forecast.getForecastDate().getDayOfWeek() + ": " + forecast.getMaxTemp() + " degrees\n";
        }
        return result;
    }
}
