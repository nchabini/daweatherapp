/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weather.weather.ui;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import weather.rest.service.RestClient;
import weather.weather.domain.Forecast;

/**
 *
 * @author nataliyachabini
 */
public class Main {
    public static void main(String[]args){
        RestClient client = new RestClient();
        System.out.println("Temp today in Kiev:"+client.getObservation("Kiev").getTemp());
          LocalDateTime dt = new Timestamp(1477160837*1000L).toLocalDateTime();
       
        System.out.println(dt);
        List<Forecast> forecasts = client.loadWeatherForecasts("Kiev");
        String result = "";
        for (Forecast f : forecasts) {
            result += f.getForecastDate() + ": " + f.getMaxTemp() + "\n";
        }
        System.out.println("Forecast" + result);
    }
    
}
